const css = document.querySelector('h3');
const color1 = document.getElementById('color1');
const color2 = document.getElementById('color2');
const gradient = document.getElementById('gradient');
const button = document.querySelector('.btn');

function setBackgroundColor() {
  gradient.style.background = 'linear-gradient(to right, ' + color1.value + ' , ' + color2.value + ')';
  css.textContent = gradient.style.background;
}


function generateRandomColor() {
  console.log(Math.round(Math.random()*255));
  const r = Math.round(Math.random()* 255);
  const g = Math.round(Math.random()* 255);
  const b = Math.round(Math.random()* 255);
  return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function setRandomGradient() {
  color1.value = generateRandomColor();
  color2.value = generateRandomColor();
  setBackgroundColor();
}

setBackgroundColor();
button.addEventListener('click', setRandomGradient);
color1.addEventListener('input', setBackgroundColor);
color2.addEventListener('input', setBackgroundColor);
